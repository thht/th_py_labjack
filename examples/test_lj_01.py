import th_py_labjack
import time

print(th_py_labjack.has_labjack())

lj_trigger = th_py_labjack.LabjackTrigger(
    th_py_labjack.ChannelGroup.FIO
)

time.sleep(1)

#lj_trigger.prepare_trigger(255, 0)
lj_trigger.prepare_trigger(255, 2)
#lj_trigger.prepare_trigger(255, 10)
#lj_trigger.prepare_trigger(255, 15)
#lj_trigger.prepare_trigger(255, 20)
#lj_trigger.prepare_trigger(255, 150)
lj_trigger.fire_trigger()

time.sleep(20)

del lj_trigger
